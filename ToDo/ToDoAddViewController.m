//
//  ToDoAddViewController.m
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ToDoAddViewController.h"
#import "Task.h"

@interface ToDoAddViewController ()

@property (weak, nonatomic) IBOutlet UITextField *taskLabelInput;
@property (weak, nonatomic) IBOutlet UITextField *taskDescriptionInput;

@end

@implementation ToDoAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)addButtonClicked:(id)sender {
    [self.data addObject:[Task taskWithLabel:self.taskLabelInput.text andDescription:self.taskDescriptionInput.text]];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
