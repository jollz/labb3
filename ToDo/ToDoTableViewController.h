//
//  ToDoTableViewController.h
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToDoTableViewController : UITableViewController

@property (nonatomic) NSMutableArray* tasks;

@end
