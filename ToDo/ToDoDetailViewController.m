//
//  ToDoDetailViewController.m
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ToDoDetailViewController.h"
#import "ToDoTableViewController.h"

@interface ToDoDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *taskLabel;
@property (weak, nonatomic) IBOutlet UITextView *taskDescription;

@end

@implementation ToDoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.taskLabel.text = self.selectedTask.taskLabel;
    self.taskDescription.text = self.selectedTask.taskDescription;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)removeClicked:(id)sender {
    [self.data removeObjectAtIndex:self.selectedIndex];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
