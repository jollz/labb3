//
//  ToDoDetailViewController.h
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "task.h"

@interface ToDoDetailViewController : UIViewController

@property (nonatomic) Task* selectedTask;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic) NSMutableArray* data;

@end
