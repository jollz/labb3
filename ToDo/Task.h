//
//  Task.h
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property (nonatomic) NSString* taskLabel;
@property (nonatomic) NSString* taskDescription;

+(Task*) taskWithLabel:(NSString*)label andDescription:(NSString*)description;

@end
