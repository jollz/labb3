//
//  ToDoTableViewController.m
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ToDoTableViewController.h"
#import "ToDoDetailViewController.h"
#import "ToDoAddViewController.h"
#import "Task.h"

@interface ToDoTableViewController ()

@property (strong, nonatomic) IBOutlet UITableView *ToDoTable;

@end

@implementation ToDoTableViewController

-(NSMutableArray*)tasks{
    if(!_tasks){
        _tasks = [@[
                   [Task taskWithLabel:@"Example: Clean your room!" andDescription:@"Don't forget the windows!"],
                   [Task taskWithLabel:@"Example: Wash the car!" andDescription:@"Don't forget the tires!"],
                   [Task taskWithLabel:@"Example: Cook food!" andDescription:@"I wan't noodles!"]
                   ] mutableCopy];
    }
    return _tasks;
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* cellIdentifier = @"ToDoCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Task* task = self.tasks[indexPath.row];
    
    cell.textLabel.text = task.taskLabel;
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"Detail"]){
        ToDoDetailViewController* controller = [segue destinationViewController];
        UITableViewCell* cell = sender;
        NSIndexPath* indexPath = [self.tableView indexPathForCell:sender];
        id selectedTask = [self.tasks objectAtIndex:indexPath.row];
        controller.selectedTask = selectedTask;
        controller.selectedIndex = indexPath.row;
        controller.data = self.tasks;
        controller.title = cell.textLabel.text;
    } else {
        ToDoAddViewController* controller = [segue destinationViewController];
        controller.data = self.tasks;
        controller.title = @"Add task";
    }
}

@end
