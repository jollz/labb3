//
//  Task.m
//  ToDo
//
//  Created by dronnefjord on 2015-02-07.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "Task.h"

@implementation Task

+(Task*) taskWithLabel:(NSString*)label andDescription:(NSString*)description{
    Task* newTask = [[Task alloc] init];
    newTask.taskDescription = description;
    newTask.taskLabel = label;
    return newTask;
}

@end
